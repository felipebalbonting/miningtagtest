# Miningtagtest

Prueba técnica para la compañía Digital Talents. Consiste principalmente en el consumo de datos a través de API Rest, transformación de los datos obtenidos para finalmente mostrarlos en tablas.

## Correr

Para correr en un entorno de desarrollo, correr `ng serve -o`. En caso de querer compilar el proyecto, ejecutar `ng build`.
