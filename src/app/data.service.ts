import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  getFirstData(): Observable<any> {
    return this.http.get('http://patovega.com/prueba_frontend/array.php');
  }

  getSecondData(): Observable<any> {
    return this.http.get('http://patovega.com/prueba_frontend/dict.php');
  }
}
