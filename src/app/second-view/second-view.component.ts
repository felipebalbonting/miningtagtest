import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.scss']
})
export class SecondViewComponent implements OnInit {

  // Estado de cargando
  isLoading = false;

  // Números
  alphabet: string[] = [];

  // Párrafos
  paragraphs: string[] = [];

  // Si hay error
  error: boolean;

  constructor(private modalService: NgbModal, private dataService: DataService) {}

  ngOnInit(): void {
    let i = 10;
    while (i < 36) {
      this.alphabet.push((i).toString(36));
      i++;
    }
  }

  /**
   * Abre el modal al ejecutarse
   * @param content Contenido a mostrar dentro del modal
   */
  async open(content): Promise<void> {
    this.isLoading = true;
    await this.getData(); // Llamada a API
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true, size: 'xl'});
  }

  /**
   * Realiza llamada al servicio para traer los datos de la API
   */
  getData(): void {
    this.dataService.getSecondData()
    .subscribe(
      (res: any) => {
        if (res.error !== '' || !res.success) {
          this.error = true;
          throw new Error('Datos incorrectos');
          return;
        }

        this.error = false;
        for (const el of JSON.parse(res.data)) {
          this.paragraphs.push(el.paragraph);
        }
        this.isLoading = false;
        return;

      },
      (err) => {
        this.error = true;
        console.error(err);
        throw new Error('Ocurrió un error al ejecutar la petición. Intenta nuevamente.');
        return;
      }
    );
  }

  /**
   * Cuenta las apariciones de una string dentro de otro
   * @param paragraph Párrafo
   * @param letter Letra a buscar
   */
  countLetters(paragraph: string, letter: string): number {
    return (paragraph.match(new RegExp(letter, 'g')) || []).length;
  }

  /**
   * Guarda todos los números dentro de un array y los suma
   * ! Si un número se antepone de un guión, es positivo (ej. -3 sería 3)
   * ! Si un número contiene 0s, se ignoran y se separa (ej. 6007 sería 6 y 7)
   * @param paragraph Párrafo
   */
  calculateNumber(paragraph: string): number {
    let numbers: number[];
    const arr = paragraph.match(/[1-9]+/g);
    if (arr !== null) {
      numbers = arr.map(Number);
    } else {
      return 0;
    }

    return numbers.reduce((a, b) => a + b);

  }

}
