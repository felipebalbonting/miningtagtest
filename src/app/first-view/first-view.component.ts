import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { throwError } from 'rxjs';
import { FirstData } from './first-data.model';

@Component({
  selector: 'app-first-view',
  templateUrl: './first-view.component.html',
  styleUrls: ['./first-view.component.scss']
})

export class FirstViewComponent implements OnInit {

  // Estado de cargando
  isLoading = false;

  // Números
  numbers: number[];

  // Números sin duplicados
  uniqueNumbers: number[];

  // Datos tabla
  arr: FirstData[];

  // Si hay error
  error: boolean;

  constructor(private modalService: NgbModal, private dataService: DataService) {}

  ngOnInit(): void {
  }

  /**
   * Abre el modal al ejecutarse
   * @param content Contenido a mostrar dentro del modal
   */
  async open(content): Promise<void> {
    this.isLoading = true;
    this.arr = [];
    await this.getData(); // Llamada a API
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true});
  }

  /**
   * Realiza llamada al servicio para traer los datos de la API
   */
  getData(): void {
    this.dataService.getFirstData()
    .subscribe(
      (res: any) => {
        if (res.error !== '' || !res.success) {
          this.error = true;
          throw new Error('Datos incorrectos');
          return;
        }

        this.error = false;
        this.numbers = res.data;
        this.uniqueNumbers = [...new Set(this.numbers)];
        this.generateTable(); // Genera datos tabla
        return;

      },
      (err) => {
        this.error = true;
        console.error(err);
        throw new Error('Ocurrió un error al ejecutar la petición. Intenta nuevamente.');
        return;
      }
    );
  }

  /**
   * Retorna el número de ocurrencias de un número dentro de un array
   * @param arr Arreglo
   * @param num Número a evaluar
   */
  countOcurrences(num: number): number {
    return this.numbers.reduce((a, n) => (n === num ? a + 1 : a), 0);
  }

  /**
   * Genera valores para mostrar en la tabla
   */
  generateTable(): void {
    let data: FirstData;
    for (const n of this.numbers) {
      data = {
        num: n,
        ocurr: this.countOcurrences(n),
        firstIdx: this.numbers.indexOf(n),
        lastIdx: this.numbers.lastIndexOf(n)
      };

      // Solo agrega si no está en el arreglo el número
      if (!this.arr.find(element => element.num === n)) {
        this.arr.push(data);
      }
    }
    this.isLoading = false;
  }

  /**
   * Algoritmo bubblesort para ordenar de manera ascendente
   * @param numbers Array con números
   */
  bubbleSort(numbers: number[]): string {
    let done = false;
    while (!done) {
      done = true;
      for (let i = 1; i < numbers.length; i++) {
        if (numbers[i - 1] > numbers[i]) {
          done = false;
          const tmp = numbers[i - 1];
          numbers[i - 1] = numbers[i];
          numbers[i] = tmp;
        }
      }
    }
    return numbers.toString();
  }

}
