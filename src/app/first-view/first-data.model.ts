export interface FirstData {
  num: number;
  ocurr: number;
  firstIdx: number;
  lastIdx: number;
}